@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <span style="margin-right : 150px">Users</span>
                    <a href="{{ route('users.create') }}"  class="btn btn-primary btn-sm pull-right">Add User</a>
                </div>

                <div class="card-body">
                    <div class="container">
                        @if (session('status'))
                            @if(session('status') == 'success')
                                <div class="alert alert-success">
                                    {{ session('message') }}
                                </div>
                            @else
                                <div class="alert alert-danger">
                                    {{ session('message') }}
                                </div>
                            @endif
                        @endif
                        <table class="table table-bordered">
                            <thead>
                                <td>Name</td>
                                <td>Action</td>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    </tr>
                                        <td>{{ $user->firstname }}</td>
                                        <td>
                                            @if(isset($deleted))
                                                <form method="POST" action="{{ route('users.restore',$user->id) }}"> 
                                                    @csrf
                                                    @method('PATCH')
                                                    <button type="submit">Restore</button>
                                                </form>
                                                <form method="POST" action="{{ route('users.delete',$user->id) }}"> 
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit">Delete</button>
                                                </form>
                                            @else

                                                <form method="POST" action="{{ route('users.destroy',$user->id) }}"> 
                                                    @csrf
                                                    @method('DELETE')
                                                    <a class="btn btn-primary btn-sm" href="{{ route('users.show',$user->id) }}">View</a> <a class="btn btn-primary btn-sm" href="{{ route('users.edit',$user->id) }}">Edit</a> <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                </form>
                                            @endif
                                        </td>
                                    <tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;
use App\Events\UserSaved;

class User extends Authenticatable
{
    use Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullnameAttribute()
    {
        return "{$this->firstname} {$this->middlename} {$this->lastname}";
    }

    public function getMiddleinitialAttribute()
    {
        $names = explode(" ",$this->middlename);
        $initials = "";
        foreach ($names as $name) {
            $initials .= substr($name,0,1).". ";
        }
        return strtoupper($initials);
    }

    public function getAvatarAttribute()
    {
        return asset('/storage/'.$this->photo);
    }

    public function details()
    {
        return $this->hasMany(\App\Detail::class);
    }

    protected $dispatchesEvents = [
        'saved' => UserSaved::class,
    ];

     
}

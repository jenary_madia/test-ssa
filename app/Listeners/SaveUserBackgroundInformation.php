<?php

namespace App\Listeners;

use App\Events\UserSaved;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Services\UserServiceInterface;

class SaveUserBackgroundInformation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    private $userService;
    public function __construct(UserServiceInterface $userService) {
        $this->userService = $userService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserSaved $event)
    {
        $this->userService->saveDetails($event->user);
    }
}

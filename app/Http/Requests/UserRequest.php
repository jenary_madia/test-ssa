<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Services\UserServiceInterface;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $user_id = $request->route('user');    
        return $this->container->make(
            UserServiceInterface::class
        )->rules($request,$user_id);
    }

    public function validationData()
    {
        $user_details = json_decode($this->user_details);
        $this->merge(['prefixname' => $user_details->prefixname]);
        $this->merge(['firstname' => $user_details->firstname]);
        $this->merge(['middlename' => $user_details->middlename]);
        $this->merge(['lastname' => $user_details->lastname]);
        $this->merge(['username' => $user_details->username]);
        $this->merge(['email' => $user_details->email]);
        $this->merge(['password' => $user_details->password]);
        $this->merge(['password_confirmation' => $user_details->password_confirmation]);
        return $this->all();
    }
}

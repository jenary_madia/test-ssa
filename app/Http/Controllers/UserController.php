<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;
use DB;
use Hash;
use Storage;
use Auth;
use App\Services\UserServiceInterface;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $userService;
    public function __construct(UserServiceInterface $userService) {
        $this->userService = $userService;
    }

    public function index()
    {
        $users = $this->userService->list();
        return view('user.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        DB::beginTransaction();
        try {
           if($request->hasFile('photo')) $path = $this->userService->upload($request->file('photo'));
           $this->userService->store([
                'prefixname' => $request->prefixname,
                'firstname' => $request->firstname,
                'middlename' => $request->middlename,
                'lastname' => $request->lastname,
                'username' => $request->username,
                'email' => $request->email,
                'photo' => ! isset($path) ?: $path,
                'password' => Hash::make($request->password),
            ]);
            DB::commit();
            return response()->json([
                'message' => 'User successfully created',
            ]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json([
                'message' => 'Something went wrong',
            ]);
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('user.show',['user' => $this->userService->find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('user.edit',['user' => $this->userService->find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            if($request->hasFile('photo')) $path = $this->userService->upload($request->file('photo'));
            $this->userService->update($id,[
                'prefixname' => $request->prefixname,
                'firstname' => $request->firstname,
                'middlename' => $request->middlename,
                'lastname' => $request->lastname,
                'username' => $request->username,
                'photo' => ! isset($path) ?: $path,
                'email' => $request->email,
            ]);
            DB::commit();
            return response()->json([
                'message' => 'User successfully updated',
            ]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json([
                'message' => 'Something went wrong',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $this->userService->destroy($id);
            DB::commit();
            return back()->with([
                'status' => 'success',
                'message' => 'User successfully deleted'
            ]);
        } catch (\Throwable $th) {
            DB::rollback();
            return back()->with([
                'status' => 'failed',
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function trashed() {
        $users = $this->userService->listTrashed();
        return view('user.index', ['users' => $users,'deleted' => true]);
    }

    public function restore($id) {
        DB::beginTransaction();
        try {
            $this->userService->restore($id);
            DB::commit();
            return back()->with([
                'status' => 'success',
                'message' => 'User successfully restored'
            ]);
        } catch (\Throwable $th) {
            DB::rollback();
            return back()->with([
                'status' => 'failed',
                'message' => 'Something went wrong'
            ]);
        }
    }

    public function delete($id) {
        DB::beginTransaction();
        try {
            $this->userService->delete($id);
            DB::commit();
            return back()->with([
                'status' => 'success',
                'message' => 'User successfully deleted permanently'
            ]);
        } catch (\Throwable $th) {
            DB::rollback();
            return back()->with([
                'status' => 'failed',
                'message' => 'Something went wrong'
            ]);
        }
    }

}

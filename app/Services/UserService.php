<?php
namespace App\Services;

use App\User;
use App\Detail;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Validation\Rule;
use Storage;

class UserService implements UserServiceInterface
{
    /**
     * The model instance.
     *
     * @var App\User
     */
    protected $model;

    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Constructor to bind model to a repository.
     *
     * @param \App\User                $model
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(User $model, Request $request)
    {
        $this->model = $model;
        $this->request = $request;
    }

    /**
     * Define the validation rules for the model.
     *
     * @param  int $id
     * @return array
     */
    public function rules($request,$id = null)
    {
        $rules = [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'username' => 'required|string|max:255|'.Rule::unique('users')->ignore($id),
            'email' => 'required|string|email|max:255|'.Rule::unique('users')->ignore($id),
            'password' => 'required|string|min:8|confirmed',
            'photo' => !$request->hasFile('photo') ? '': 'image',
        ];

        if($id)
            $rules['password'] = "string|min:8|confirmed";

        return $rules;
    }

    /**
     * Retrieve all resources and paginate.
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function list()
    {
        $users = $this->model->paginate(15)->items();
        $total = count($users);
        $currentpage = \Request::get('page', 1);
        $offset = ($currentpage * 15) - 15 ;
        $userstoshow = array_slice($users , $offset , 15);
        $p = new LengthAwarePaginator($userstoshow ,$total ,15);
        return $p;
    }

    /**
     * Create model resource.
     *
     * @param  array $attributes
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function store(array $attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * Retrieve model resource details.
     * Abort to 404 if not found.
     *
     * @param  integer $id
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function find(int $id)
    {
        try {
            $user = $this->model->with('details')->findOrFail($id);
            return $user;
        } catch (\Throwable $th) {
            return (new \Response('Model not found', 404));
        }
    }

    /**
     * Update model resource.
     *
     * @param  integer $id
     * @param  array   $attributes
     * @return boolean
     */
    public function update(int $id, array $attributes): bool
    {
        return $this->model->find($id)->update($attributes);
    }

    /**
     * Soft delete model resource.
     *
     * @param  integer|array $id
     * @return void
     */
    public function destroy($id)
    {
        return $this->model->find($id)->delete();
    }

    /**
     * Include only soft deleted records in the results.
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function listTrashed()
    {
        $users = $this->model->onlyTrashed()->paginate(15)->items();
        $total = count($users);
        $currentpage = \Request::get('page', 1);
        $offset = ($currentpage * 15) - 15 ;
        $userstoshow = array_slice($users , $offset , 15);
        $p = new LengthAwarePaginator($userstoshow ,$total ,15);
        return $p;
    }

    /**
     * Restore model resource.
     *
     * @param  integer|array $id
     * @return void
     */
    public function restore($id)
    {
        return $this->model->withTrashed()->find($id)->restore();
    }

    /**
     * Permanently delete model resource.
     *
     * @param  integer|array $id
     * @return void
     */
    public function delete($id)
    {
        return $this->model->withTrashed()->find($id)->forceDelete();
        
    }

    /**
     * Generate random hash key.
     *
     * @param  string $key
     * @return string
     */
    public function hash(string $key): string
    {
        return \Hash::make($key);
    }

    /**
     * Upload the given file.
     *
     * @param  \Illuminate\Http\UploadedFile $file
     * @return string|null
     */
    public function upload(UploadedFile $file)
    {
        return Storage::putFile('photo', $file);
    }

    public function saveDetails($user)
    {
        $data = [ 
            [
                'key' => 'Full name',
                'value' => $user->fullname,
                'type' => 'bio',
                'user_id' => $user->id,
            ],
            [
                'key' => 'Middle Initial',
                'value' => $user->middleinitial,
                'type' => 'bio',
                'user_id' => $user->id,
            ],
            [
                'key' => 'Avatar',
                'value' => $user->avatar,
                'type' => 'bio',
                'user_id' => $user->id,
            ],
            [
                'key' => 'Gender',
                'value' => $user->prefixname == 'mr' ? "Male" : "Female",
                'type' => 'bio',
                'user_id' => $user->id,
            ],
        ];
        Detail::where("user_id",$user->id)->delete();
        return Detail::insert($data);
    }
}
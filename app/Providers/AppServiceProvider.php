<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Route;
use App\Services\UserServiceInterface;
use App\Services\UserService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserServiceInterface::class, UserService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Route::macro('softDeletes', function ($prefix,$controller) {
            Route::group([
                'prefix' => $prefix,
            ], function () use ($prefix,$controller) {
                Route::get("/trashed", "$controller@trashed")->name("$prefix.trashed");
                Route::patch("/{".$prefix."}/restore", "$controller@restore")->name("$prefix.restore");
                Route::delete("/{".$prefix."}/delete", "$controller@delete")->name("$prefix.delete");
            });
        });
    }
}

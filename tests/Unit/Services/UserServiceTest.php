<?php

namespace Tests\Unit\Services;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Services\UserServiceInterface;
use App\User;
use Illuminate\Http\UploadedFile;
/**
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
class UserServiceTest extends TestCase
{
    use WithFaker;
    // function __construct() {
    //     $this->student = new Student();
    // }
    /**
     * @test
     * @return void
     */
    public function it_can_return_a_paginated_list_of_users()
    {
        $UserService = \App::make('App\Services\UserServiceInterface');
        $users = $UserService->list();
        $this->assertIsObject($users);
    }

    /**
     * @test
     * @return void
     */
    public function it_can_store_a_user_to_database()
    {
        $UserService = \App::make('App\Services\UserServiceInterface');
        $user = $UserService->store([
            'prefixname' => $this->faker->titleMale(),
            'firstname' => $this->faker->firstNameMale(),
            'lastname' => $this->faker->lastName(),
            'username' => $this->faker->userName(),
            'email' => $this->faker->safeEmail(),
            'password' => $UserService->hash('Sample'),
        ]);
        $this->assertEquals($user->wasRecentlyCreated, true);
        return $user->id;
    }

    /**
     * @test
     * @depends it_can_store_a_user_to_database
     * @return void
     */
    public function it_can_find_and_return_an_existing_user($id)
    {
        $UserService = \App::make('App\Services\UserServiceInterface');
        $user = $UserService->find($id);
        $this->assertIsObject($user);
    }

    /**
     * @test
     * @depends it_can_store_a_user_to_database
     * @return void
     */
    public function it_can_update_an_existing_user($id)
    {
        $UserService = \App::make('App\Services\UserServiceInterface');
        $user = $UserService->update($id,[
            'prefixname' => $this->faker->titleMale(),
            'firstname' => $this->faker->firstNameMale(),
            'lastname' => $this->faker->lastName(),
            'username' => $this->faker->userName(),
            'email' => $this->faker->safeEmail(),
            'password' => $UserService->hash('Sample'),
        ]);
        $this->assertTrue($user);
    }

    /**
     * @test
     * @depends it_can_store_a_user_to_database
     * @return void
     */
    public function it_can_soft_delete_an_existing_user($id)
    {
        $UserService = \App::make('App\Services\UserServiceInterface');
        $user = $UserService->destroy($id);
        $this->assertTrue($user);
    }

    /**
     * @test
     * @depends it_can_store_a_user_to_database
     * @return void
     */
    public function it_can_return_a_paginated_list_of_trashed_users($id)
    {
        $UserService = \App::make('App\Services\UserServiceInterface');
        $users = $UserService->listTrashed();
        $this->assertIsObject($users);
    }

    /**
     * @test
     * @depends it_can_store_a_user_to_database
     * @return void
     */
    public function it_can_restore_a_soft_deleted_user($id)
    {
        $UserService = \App::make('App\Services\UserServiceInterface');
        $user = $UserService->restore($id);
        $this->assertTrue($user);
    }

    /**
     * @test
     * @depends it_can_store_a_user_to_database
     * @return void
     */
    public function it_can_permanently_delete_a_soft_deleted_user($id)
    {
        $UserService = \App::make('App\Services\UserServiceInterface');
        $user = $UserService->delete($id);
        $this->assertTrue($user);
    }

    /**
     * @test
     * @return void
     */
    public function it_can_upload_photo()
    {
        $UserService = \App::make('App\Services\UserServiceInterface');
        $uploaded = $UserService->upload(UploadedFile::fake()->image('photo2.jpg'));
        \Storage::assertExists($uploaded);
    }
}

